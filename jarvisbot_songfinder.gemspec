# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "jarvisbot_songfinder/version"

Gem::Specification.new do |spec|
  spec.name          = "jarvisbot_songfinder"
  spec.version       = JarvisbotSongfinder::VERSION
  spec.authors       = ["Nick"]
  spec.email         = ["doublejarvis@gmail.com"]

  spec.summary       = "Take user input and return music track"
  spec.description   = "Gem jarvissongbot uses to take user input and transform it into queryable music track found on enabled providers (youtube, spotify), validating it afterwards"
  spec.homepage      = "https://gitlab.com/DoubleJarvis/jarvisbot_songfinder"
  spec.license       = "MIT"

  spec.required_ruby_version = ">= 2.5.0"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    # spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"

    spec.metadata["homepage_uri"] = spec.homepage
    spec.metadata["source_code_uri"] = "https://gitlab.com/DoubleJarvis/jarvisbot_songfinder"
    spec.metadata["changelog_uri"] = "https://gitlab.com/DoubleJarvis/jarvisbot_songfinder/blob/master/changelog.md"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.17"
  spec.add_development_dependency "pry"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "simplecov", "~> 0.16"
  spec.add_development_dependency "vcr"
  spec.add_development_dependency "webmock"

  spec.add_dependency "httparty", "~> 0.16"
  spec.add_dependency "rspotify", "~> 2.4.0"
  spec.add_dependency "yt", "~> 0.29.1"
end
