# frozen_string_literal: true

# :nodoc:
module JarvisbotSongfinder
  class << self
    attr_accessor :configuration
  end

  def self.configure
    self.configuration ||= Configuration.new
    yield(configuration)
  end

  # Holds the app configuration
  #   Configuration.region # => nil
  class Configuration
    attr_accessor :spotify_client_id,
                  :spotify_secret,
                  :youtube_api_key,
                  :region,
                  :length_min,
                  :length_max

    def initialize
      @spotify_client_id = nil
      @spotify_secret = nil
      @youtube_api_key = nil
      @region = nil
      @length_min = 0
      @length_max = Float::INFINITY
    end
  end
end
