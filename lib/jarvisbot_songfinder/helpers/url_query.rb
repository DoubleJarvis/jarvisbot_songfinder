# frozen_string_literal: true

module JarvisbotSongfinder
  # Responsible for handling URL based requests
  #   UrlQuery.new("https://www.youtube.com/watch?v=QSBco8kVuZM").track.length
  #   # => 172
  class UrlQuery
    def initialize(url, config: JarvisbotSongfinder.configuration)
      @config = config
      @url = url
      @provider = select_provider
    end

    def track
      @provider
    end

    private

    def select_provider
      provider = Provider.available_providers.find do |p|
        p::URL_REGEX.match @url
      end
      provider.nil? ? UnknownProvider.new : provider.new(@url, config: @config)
    end
  end
end
