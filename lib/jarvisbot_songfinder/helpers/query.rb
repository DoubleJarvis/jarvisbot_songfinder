# frozen_string_literal: true

# :nodoc:
module JarvisbotSongfinder
  require 'forwardable'

  # Responsible for selecting concrete query and delegating track methods to it
  #   Query.new("Paramore - Decode").title
  #   # => "Decode"
  class Query
    extend Forwardable

    URL_REGEX = %r{\Ahttps?:\/\/}.freeze
    attr_reader :concrete_query

    def initialize(input, config: JarvisbotSongfinder.configuration)
      @config = config
      @input = input
      @concrete_query = select_concrete_query
    end

    def_delegators  :track,
                    :length,
                    :artist,
                    :title,
                    :provider,
                    :url,
                    :valid?,
                    :explicit?,
                    :errors

    private

    def track
      @concrete_query.track
    end

    def select_concrete_query
      @input.match?(URL_REGEX) ? UrlQuery.new(@input, config: @config) : SearchQuery.new(@input, config: @config)
    end
  end
end
