# frozen_string_literal: true

module JarvisbotSongfinder
  # contains all the messages gem might use in a single place
  module ReplyMessage
    # :nodoc:
    module Request
      def self.usage
        "Usage: '!sr $url'"
      end

      def self.queue_closed
        "Song queue is closed. Please wait until Dan opens it."
      end

      def self.submitted_successfully(request)
        "Your request: #{request} successfully submitted"
      end

      def self.already_requested(request)
        "#{request} already exists in the queue"
      end

      def self.user_already_requested(request)
        "You already have #{request} in the queue, use '!replace url/search term' to replace"
      end

      def self.invalid_url(request)
        "#{request} is invalid"
      end

      def self.too_long
        "Your request is too long, please stick to videos not longer than 10 minutes"
      end

      def self.too_short
        "Your request is too short, troll requests or gnoming will not fly here"
      end

      def self.invalid_category
        "Your request is not in Music category"
      end

      def self.replaced_successfully(request)
        "Your previous request was replaced with #{request}"
      end

      def self.nothing_to_replace
        "Nothing to replace. Use '!sr url/search term' to request something"
      end

      def self.nothing_to_remove
        "Nothing to remove. Use '!sr url/search term' to request something"
      end

      def self.removed_successfully
        "Your request removed from the queue successfully"
      end

      def self.region_restricted(region)
        "This video is not available in the #{region}"
      end

      def self.invalid_video_id
        "Invalid video ID"
      end

      def self.promoted_successfully(request)
        "Request: #{request} was promoted successfully"
      end

      def self.promoted_unsuccessfully
        "Cannot promote"
      end

      def self.demoted_successfully(request)
        "Request: #{request} was demoted successfully"
      end

      def self.demoted_unsuccessfully
        "Cannot demote"
      end

      def self.unknown_provider
        "Unknown provider. Available providers: " +
          JarvisbotSongfinder::Provider.available_providers.to_s
      end

      def self.song_banned
        "Song was banned by Dan, request something else"
      end
    end

    # :nodoc:
    module Song
      def self.currently_playing(song)
        "#{song} is currently playing"
      end

      def self.not_playing
        "Nothing is currently playing"
      end
    end

    # :nodoc:
    module Search
      def self.not_found
        "Nothing is found, check your spelling"
      end
    end
  end
end
