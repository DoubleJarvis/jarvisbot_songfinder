# frozen_string_literal: true

module JarvisbotSongfinder
  # Reponsible for handling search (words) based requests
  #   JarvisbotSongfinder::SearchQuery.new("Paramore Misery").track.length
  #   # => 211
  class SearchQuery
    def initialize(query, config: JarvisbotSongfinder.configuration)
      @query = query
      @config = config
      @provider = select_provider
    end

    def track
      @provider
    end

    private

    def select_provider
      provider = Provider.available_providers.find do |p|
        p.from_search @query, config: @config
      end
      provider.nil? ? UnknownSearchProvider.new : provider.from_search(@query, config: @config)
    end
  end
end
