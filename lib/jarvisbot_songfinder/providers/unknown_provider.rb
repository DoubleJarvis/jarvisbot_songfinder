# frozen_string_literal: true

module JarvisbotSongfinder
  # NullObject for handling cases where none of the providers matched request
  # with their URL_REGEX
  class UnknownProvider
    attr_reader :errors

    def initialize
      @errors = [ReplyMessage::Request.unknown_provider]
    end

    def valid?
      false
    end
  end
end
