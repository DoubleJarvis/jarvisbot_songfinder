# frozen_string_literal: true

module JarvisbotSongfinder
  class YoutubeAPI < Provider
    Provider.available_providers << self
    ID_REGEX = /\A((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu\.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?/.freeze
    URL_REGEX = /youtube\.com|youtu\.be/.freeze

    def initialize(url, config: JarvisbotSongfinder.configuration)
      super()
      @config = config
      @track_id = get_track_id(url)
      @track = Yt::Video.new(id: @track_id) # unless @track_id.nil?
      check_id_validity
      valid? unless @errors.any?
    end

    def self.from_search(query, config: JarvisbotSongfinder.configuration)
      @config = config
      videos = Yt::Collections::Videos.new
      params = { q: query, order: "relevance", video_category_id: 10 }
      if track_id = videos.where(params)&.first&.id
        link = "https://www.youtube.com/watch?v=#{track_id}"
        return new(link)
      else
        return nil
      end
    end

    def title
      @track.title
    end

    def length
      @track.duration
    end

    def artist
      @track.channel_title
    end

    def url
      "https://www.youtube.com/watch?v=#{@track_id}"
    end

    def provider
      "youtube"
    end

    def explicit?
      # no way to know
      false
    end

    private

    # :nocov:
    def get_track_id(url)
      if !ID_REGEX.match(url)
        add_error ReplyMessage::Request.invalid_video_id
        nil
      else
        ID_REGEX.match(url)[5]
      end
    end

    def check_id_validity
      @track.empty?
    rescue Yt::Errors::NoItems => e
      add_error ReplyMessage::Request.invalid_video_id
    end


    def in_music_category?
      if @track.category_id == "10"
        true
      else
        add_error ReplyMessage::Request.invalid_category
        false
      end
    end

    def available_in_region?
      return true if !listed? || in_allowed_list? || !in_blocked_list?
      add_error ReplyMessage::Request.region_restricted(@region)
      return false
    end

    def listed?
      @track.content_detail.data&.dig('regionRestriction')
    end

    def in_allowed_list?
      if allowed_list = @track.content_detail.data&.dig('regionRestriction', 'allowed')
        allowed_list.include?(@config.region)
      else
        false
      end
    end

    def in_blocked_list?
      if blocked_list = @track.content_detail.data&.dig('regionRestriction', 'blocked')
        blocked_list.include?(@config.region)
      else
        false
      end
    end
  end
  # :nocov:
end
