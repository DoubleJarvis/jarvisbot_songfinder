# frozen_string_literal: true

module JarvisbotSongfinder
  class SpotifyAPI < Provider
    Provider.available_providers << self
    ID_REGEX = %r{\Ahttps?:\/\/(?:embed\.|open\.)(?:spotify\.com\/)(?:track\/|\?uri=spotify:track:)((\w|-){22})}.freeze
    URL_REGEX = %r{spotify\.com/}.freeze

    def initialize(url, config: JarvisbotSongfinder.configuration)
      super()
      @config = config
      @track_id = get_track_id(url)
      check_id_validity
      valid? unless @errors.any?
    end

    def self.from_search(query, config: JarvisbotSongfinder.configuration)
      @config = config
      results = RSpotify::Track.search(query, market: @config.region)
      if results.any?
        link = "https://open.spotify.com/track/#{results.first.id}"
        return new(link)
      else
        return nil
      end
    end

    def length
      @track.duration_ms / 1000
    end

    def title
      @track.name
    end

    def artist
      @track.artists.first.name
    end

    def url
      "https://open.spotify.com/track/#{@track.id}"
    end

    def provider
      "spotify"
    end

    def explicit?
      @track.explicit
    end

    private

    # :nocov:
    def get_track_id(url)
      if !ID_REGEX.match(url)
        add_error ReplyMessage::Request.invalid_video_id
        nil
      else
        ID_REGEX.match(url)[1]
      end
    end

    def check_id_validity
      @track = RSpotify::Track.find(@track_id, market: @config.region)
    rescue RestClient::BadRequest
      add_error ReplyMessage::Request.invalid_video_id
    end
    # :nocov:

    def in_music_category?
      true
    end

    def available_in_region?
      return true if @track.available_markets.empty?

      if @track.available_markets.include? @config.region
        return true
      else
        add_error ReplyMessage::Request.region_restricted(@config.region)
        return false
      end
    end
  end
end
