# frozen_string_literal: true

module JarvisbotSongfinder
  # Pseudo-provider for internal use, pretty much a remnant of the past
  # i'm too lazy to extract into my rails app
  class Bandrequest < Provider
    def initialize(band)
      super()
      @band = band
    end

    def length
      0
    end

    def title
      ""
    end

    def artist
      @band
    end

    def provider
      "bandrequest"
    end

    def url
      "https://www.last.fm/search?q=#{@band}"
    end

    def explicit?
      false
    end

    def valid?
      true
    end
  end
end
