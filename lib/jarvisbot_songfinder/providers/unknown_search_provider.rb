# frozen_string_literal: true

module JarvisbotSongfinder
  # NullObject for handling cases where searching any other provider did not
  # found anything
  class UnknownSearchProvider
    attr_reader :errors

    def initialize
      @errors = [ReplyMessage::Search.not_found]
    end

    def valid?
      false
    end
  end
end
