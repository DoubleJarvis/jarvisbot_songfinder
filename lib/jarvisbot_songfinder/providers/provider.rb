# frozen_string_literal: true

module JarvisbotSongfinder
  # Abstract class for concrete providers to inherit from (Template Method Pattern)
  class Provider
    @available_providers ||= []
    class << self
      attr_accessor :available_providers
    end

    attr_reader :errors
    def initialize(config: JarvisbotSongfinder.configuration)
      @config = config
      @errors = []
    end

    def length
      raise NotImplementedError
    end

    def title
      raise NotImplementedError
    end

    def artist
      raise NotImplementedError
    end

    def provider
      raise NotImplementedError
    end

    def valid?
      return false if @errors.any?

      available_in_region? && length_valid? && in_music_category?
    end

    def url
      raise NotImplementedError
    end

    def explicit?
      raise NotImplementedError
    end

    private

    def add_error(error)
      @errors << error unless @errors.include? error
    end

    def available_in_region?
      raise NotImplementedError
    end

    def length_valid?
      not_too_long? && not_too_short?
    end

    def not_too_long?
      unless length < @config.length_max
        add_error(JarvisbotSongfinder::ReplyMessage::Request.too_long)
      end
      length < @config.length_max
    end

    def not_too_short?
      unless length > @config.length_min
        add_error(JarvisbotSongfinder::ReplyMessage::Request.too_short)
      end
      length > @config.length_min
    end

    def in_music_category?
      raise NotImplementedError
    end

    def get_track
      raise NotImplementedError
    end
  end
end
