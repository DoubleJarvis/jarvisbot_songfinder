# frozen_string_literal: true

# :nodoc:
module JarvisbotSongfinder
  require "jarvisbot_songfinder/version"
  require "jarvisbot_songfinder/configuration"

  require "jarvisbot_songfinder/helpers/query"
  require "jarvisbot_songfinder/helpers/url_query"
  require "jarvisbot_songfinder/helpers/search_query"
  require "jarvisbot_songfinder/helpers/reply_message"

  require "jarvisbot_songfinder/providers/provider"
  require "jarvisbot_songfinder/providers/unknown_search_provider"
  require "jarvisbot_songfinder/providers/unknown_provider"

  require "yt"
  require "rspotify"
  require "jarvisbot_songfinder/providers/spotify_api"
  require "jarvisbot_songfinder/providers/youtube_api"
  require "jarvisbot_songfinder/providers/bandrequest"

  class Error < StandardError; end
end
