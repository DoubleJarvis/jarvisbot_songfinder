# JarvisbotSongfinder

Takes search query / URL and searches for corresponding track on Youtube/Spotify validating it against configured restrictions (length, region availability)

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'jarvisbot_songfinder'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install jarvisbot_songfinder

### Configure it
```ruby
JarvisbotSongfinder.configure do |c|
  c.spotify_client_id = ENV["SPOTIFY_CLIENT_ID"]
  c.spotify_secret    = ENV["SPOTIFY_SECRET"]
  c.youtube_api_key   = ENV["YOUTUBE_API_KEY"]
  c.region            = "US"
end

# TODO: fix needing to manually configure those 
RSpotify.authenticate(JarvisbotSongfinder.configuration.spotify_client_id, JarvisbotSongfinder.configuration.spotify_secret)
Yt.configuration.api_key = JarvisbotSongfinder.configuration.youtube_api_key

```

Running `bundle exec rspec` expects those environmental variables to be set or it will fail the tests
```
ENV["SPOTIFY_CLIENT_ID"]
ENV["SPOTIFY_SECRET"]
ENV["YOUTUBE_API_KEY"]
```

## Usage

Intended use cases: playlist building tools, chatbots, informational apps

Find a song via search query
```ruby
track = JarvisbotSongfinder::Query.new("mogwai take me somewhere nice")
track.provider # => "spotify"
track.url      # => "https://open.spotify.com/track/1ZBkR2W1rHhoKJIWbcfaaD"
track.artist   # => "Mogwai"
track.title    # => "Take Me Somewhere Nice"
track.length   # => 417
track.errors   # => []
track.valid?   # => true
```

Find a song via URL
```ruby
track = JarvisbotSongfinder::Query.new("https://open.spotify.com/track/0CpaNwilKJGqn9WF6K6aRV")
track.provider # => "spotify"
track.url      # => "https://open.spotify.com/track/0CpaNwilKJGqn9WF6K6aRV"
track.artist   # => "65daysofstatic"
track.title    # => "Debutante"
track.length   # => 441
track.errors   # => ["This video is not available in the US"]
track.valid?   # => false
```

Trying erroneous inputs
```ruby
JarvisbotSongfinder::Query.new(" dsfjdsk jflsdjf lkjsdkf jlksdf jljsf ll ll").errors
# => ["Nothing is found, check your spelling"]
JarvisbotSongfinder::Query.new("https://example.com/track.mp3").errors
# => ["Unknown provider. Available providers: [Spotify]"]
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on [Gitlab](https://gitlab.com/DoubleJarvis/jarvisbot_songfinder).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
