# frozen_string_literal: true

RSpec.describe "JarvisbotSongfinder.configuration" do
  subject { JarvisbotSongfinder.configuration }
  context "when configuration block provided" do
    it "has a region" do
      expect(subject.region).not_to be_nil
    end
    it "has a spotify client id" do
      expect(subject.spotify_client_id).not_to be_nil
    end
    it "has a spotify secret" do
      expect(subject.spotify_secret).not_to be_nil
    end
    it "has a youtube api key" do
      expect(subject.youtube_api_key).not_to be_nil
    end
    context "with default length limits" do
      it "has a lower limit of 0" do
        expect(subject.length_min).to eq(0)
      end
      it "has an upper limit of infinity" do
        expect(subject.length_max).to eq(Float::INFINITY)
      end
    end
    context "when length limits provided" do
      before do
        JarvisbotSongfinder.configure do |c|
          c.length_min = 90
          c.length_max = 600
        end
      end
      it "has a lower limit" do
        expect(subject.length_min).to eq(90)
      end
      it "has an upper limit" do
        expect(subject.length_max).to eq(600)
      end
    end
  end
end
