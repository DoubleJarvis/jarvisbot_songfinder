# frozen_string_literal: true

RSpec.describe JarvisbotSongfinder::ReplyMessage do
  describe JarvisbotSongfinder::ReplyMessage::Request do
    context "responds to" do
      it "queue closed" do
        expect(JarvisbotSongfinder::ReplyMessage::Request.queue_closed).not_to be_nil
      end
      it "empty request" do
        expect(JarvisbotSongfinder::ReplyMessage::Request.usage).not_to be_nil
      end
      it "successful submission" do
        expect(JarvisbotSongfinder::ReplyMessage::Request.submitted_successfully("req")).not_to be_nil
      end
      it "already existing request" do
        expect(JarvisbotSongfinder::ReplyMessage::Request.already_requested("req")).not_to be_nil
      end
      it "user already has a request in the queue" do
        expect(JarvisbotSongfinder::ReplyMessage::Request.user_already_requested("test by testytest")).not_to be_nil
      end
      it "invalid url provided" do
        expect(JarvisbotSongfinder::ReplyMessage::Request.invalid_url("invalidreq")).not_to be_nil
      end
      it "song too long" do
        expect(JarvisbotSongfinder::ReplyMessage::Request.too_long).not_to be_nil
      end
      it "invalid category" do
        expect(JarvisbotSongfinder::ReplyMessage::Request.invalid_category).not_to be_nil
      end
      it "successful replacement" do
        expect(JarvisbotSongfinder::ReplyMessage::Request.replaced_successfully("req")).not_to be_nil
      end
      it "nothing to replace" do
        expect(JarvisbotSongfinder::ReplyMessage::Request.nothing_to_replace).not_to be_nil
      end
      it "nothing to remove" do
        expect(JarvisbotSongfinder::ReplyMessage::Request.nothing_to_remove).not_to be_nil
      end
      it "successful removal" do
        expect(JarvisbotSongfinder::ReplyMessage::Request.removed_successfully).not_to be_nil
      end
      it "region restricted" do
        expect(JarvisbotSongfinder::ReplyMessage::Request.region_restricted("US")).not_to be_nil
      end
      it "invalid video ID" do
        expect(JarvisbotSongfinder::ReplyMessage::Request.invalid_video_id).not_to be_nil
      end
      it "successful promotion" do
        expect(JarvisbotSongfinder::ReplyMessage::Request.promoted_successfully("req"))
          .not_to be_nil
      end
      it "unsuccessful promotion" do
        expect(JarvisbotSongfinder::ReplyMessage::Request.promoted_unsuccessfully).not_to be_nil
      end
      it "successful demotion" do
        expect(JarvisbotSongfinder::ReplyMessage::Request.demoted_successfully("req")).not_to be_nil
      end
      it "unsuccessful demotion" do
        expect(JarvisbotSongfinder::ReplyMessage::Request.demoted_unsuccessfully).not_to be_nil
      end

      it "unknown provider" do
        expect(JarvisbotSongfinder::ReplyMessage::Request.unknown_provider).not_to be_nil
      end

      it "song is banned" do
        expect(JarvisbotSongfinder::ReplyMessage::Request.song_banned).not_to be_nil
      end
    end
  end
  describe JarvisbotSongfinder::ReplyMessage::Song do
    context "responds to" do
      it "something is currently playing" do
        expect(JarvisbotSongfinder::ReplyMessage::Song.currently_playing("req")).not_to be_nil
      end
      it "nothing is currently playing" do
        expect(JarvisbotSongfinder::ReplyMessage::Song.not_playing).not_to be_nil
      end
    end
  end
  describe JarvisbotSongfinder::ReplyMessage::Search do
    context "responds to" do
      it "not found" do
        expect(JarvisbotSongfinder::ReplyMessage::Search.not_found).not_to be_nil
      end
    end
  end
end
