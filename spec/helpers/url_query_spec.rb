# frozen_string_literal: true

module JarvisbotSongfinder
  RSpec.describe JarvisbotSongfinder::UrlQuery do
    context "when provider is known" do
      let(:url_query) do
        VCR.use_cassette("url_query:known_provider") do
          described_class.new("https://www.youtube.com/watch?v=l7Z17VsymPc")
        end
      end
      it "#track returns right provider" do
        expect(url_query.track.class).to eq(JarvisbotSongfinder::YoutubeAPI)
      end
    end
    context "when provider is unknown" do
      let(:url_query) do
        VCR.use_cassette("url_query:unknown_provider") do
          described_class.new("https://www.completebollocks.com/")
        end
      end
      it "#track returns UnknownProvider" do
        expect(url_query.track.class).to eq(JarvisbotSongfinder::UnknownProvider)
      end
    end
  end
end
