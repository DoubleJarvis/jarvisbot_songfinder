# frozen_string_literal: true

RSpec.describe JarvisbotSongfinder::SearchQuery do
  describe "#track" do
    context "when resource exists" do
      context "on preferred provider" do
        let(:preferred_search_query) do
          VCR.use_cassette("search_query:preferred_provider") do
            described_class.new("adele hello")
          end
        end
        it "returns SpotifyAPI instance" do
          expect(preferred_search_query.track.class).to eq(JarvisbotSongfinder::SpotifyAPI)
        end
        context "and on auxiliary provider" do
          let(:search_query) do
            VCR.use_cassette("search_query:preferred_and_auxiliary") do
              described_class.new("In Flames - Alias")
            end
          end
          it "still returns preferred provider" do
            expect(search_query.track.class).to eq(JarvisbotSongfinder::SpotifyAPI)
          end
        end
      end
      context "just on auxiliary provider" do
        let(:search_query) do
          VCR.use_cassette("search_query:auxiliary_provider") do
            described_class.new("Skinnerhate - Until The Last Time")
          end
        end
        it "returns YoutubeAPI instance" do
          expect(search_query.track.class).to eq(JarvisbotSongfinder::YoutubeAPI)
        end
      end
    end
    context "when resource doesn't exist" do
      let(:search_query) do
        VCR.use_cassette("search_query:not_found") do
          described_class.new("abracadabrarandomshit")
        end
      end
      it "returns UnknownSearchProvider" do
        expect(search_query.track.class).to eq(JarvisbotSongfinder::UnknownSearchProvider)
      end
    end
  end
end
