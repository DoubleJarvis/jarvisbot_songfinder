# frozen_string_literal: true

RSpec.describe JarvisbotSongfinder::Query do
  context "when url provided" do
    let(:query) do
      VCR.use_cassette("query:youtube_url") do
        described_class.new("https://www.youtube.com/watch?v=nuodvDQKRAU")
      end
    end
    it "returns UrlQuery instance" do
      expect(query.concrete_query.class).to eq(JarvisbotSongfinder::UrlQuery)
    end
  end
  context "when text provided" do
    let(:query) do
      VCR.use_cassette("query:text") do
        described_class.new("Some Text")
      end
    end
    it "returns UrlQuery instance" do
      expect(query.concrete_query.class).to eq(JarvisbotSongfinder::SearchQuery)
    end
  end
  describe "has delegated #track methods" do
    let(:query) do
      VCR.use_cassette("query:track_delegation") do
        described_class.new("In Flames - Alias")
      end
    end
    it "has a length" do
      expect(query.length).not_to be_nil
    end
    it "has a title" do
      expect(query.title).not_to be_nil
    end
    it "has an artist" do
      expect(query.artist).not_to be_nil
    end
    it "has a provider" do
      expect(query.provider).not_to be_nil
    end
    it "responds to #valid?" do
      expect(query.valid?).not_to be_nil
    end
    it "has a url" do
      expect(query.url).not_to be_nil
    end
    it "responds to #explicit?" do
      expect(query.explicit?).not_to be_nil
    end
    it "responds to #errors" do
      expect(query.errors).not_to be_nil
    end
  end

  context "when custom config injected" do
    Conf = Struct.new(:region, :length_min, :length_max)
    context "when track is valid" do
      let(:config) { Conf.new("NL", 0, 600) }
      let(:query) do
        VCR.use_cassette("query:customconfig") do
          described_class.new("https://open.spotify.com/track/2oL0JRZnH7ADg3qUH7P8mw", config: config)
        end
      end
      it "is valid" do
        expect(query.valid?).to be true
      end
    end
    context "when track is invalid" do
      let(:config) { Conf.new("NL", 0, 30) }
      let(:query) do
        VCR.use_cassette("query:customconfig_short_track") do
          described_class.new("https://open.spotify.com/track/2oL0JRZnH7ADg3qUH7P8mw", config: config)
        end
      end
      it "is invalid" do
        expect(query.valid?).to be false
      end
    end
  end
end
