# frozen_string_literal: true

require 'simplecov'
SimpleCov.start

require "bundler/setup"
require "jarvisbot_songfinder"
require "webmock"
require "webmock/rspec"
require "vcr"
WebMock.disable_net_connect!(allow_localhost: true)

JarvisbotSongfinder.configure do |c|
  c.spotify_client_id = ENV["SPOTIFY_CLIENT_ID"]
  c.spotify_secret    = ENV["SPOTIFY_SECRET"]
  c.youtube_api_key   = ENV["YOUTUBE_API_KEY"]
  c.region            = "US"
end

RSpotify.authenticate(JarvisbotSongfinder.configuration.spotify_client_id, JarvisbotSongfinder.configuration.spotify_secret)
Yt.configuration.api_key = JarvisbotSongfinder.configuration.youtube_api_key

VCR.configure do |config|
  config.cassette_library_dir = 'fixtures/vcr_cassettes'
  config.hook_into :webmock
end

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end
