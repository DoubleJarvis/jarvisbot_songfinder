# frozen_string_literal: true

module JarvisbotSongfinder
  RSpec.describe JarvisbotSongfinder::Provider do
    let(:provider) { described_class.new }
    it "raises NotImplementedError on #get_track" do
      expect { provider.send(:get_track) }.to raise_error(NotImplementedError)
    end
    it "raises NotImplementedError on #length" do
      expect { provider.send(:length) }.to raise_error(NotImplementedError)
    end
    it "raises NotImplementedError on #title" do
      expect { provider.send(:title) }.to raise_error(NotImplementedError)
    end
    it "raises NotImplementedError on #artist" do
      expect { provider.send(:artist) }.to raise_error(NotImplementedError)
    end
    it "raises NotImplementedError on #valid?" do
      expect { provider.send(:valid?) }.to raise_error(NotImplementedError)
    end
    it "raises NotImplementedError on #url" do
      expect { provider.send(:url) }.to raise_error(NotImplementedError)
    end
    it "raises NotImplementedError on #available_in_region?" do
      expect { provider.send(:available_in_region?) }.to raise_error(NotImplementedError)
    end
    it "raises NotImplementedError on #not_too_long?" do
      expect { provider.send(:not_too_long?) }.to raise_error(NotImplementedError)
    end
    it "raises NotImplementedError on #in_music_category?" do
      expect { provider.send(:in_music_category?) }.to raise_error(NotImplementedError)
    end
    it "maintains available_providers list" do
      expect(provider.class.available_providers).to be_kind_of(Array)
    end
    it "raises NotImplementedError on #provider" do
      expect { provider.send(:provider) }.to raise_error(NotImplementedError)
    end
    it "raises NotImplementedError on #explicit?" do
      expect { provider.send(:explicit?) }.to raise_error(NotImplementedError)
    end

    describe "#length" do
      let(:klass) do
        # pretend we're an implementation
        Class.new(Provider) do
          def length
            300
          end

          def valid?
            not_too_long? && not_too_short?
          end
        end
      end
      context "with default configuration" do
        before do
          # make sure configuration initialized
          JarvisbotSongfinder.configure do |c|
            c.region = "US"
          end
        end
        it "is has no error" do
          expect(klass.new.errors).to be_empty
        end
      end
      context "with restricted length" do
        context "when too short" do
          before do
            # make sure configuration initialized
            JarvisbotSongfinder.configure do |c|
              c.length_min = 900
            end
          end
          after do
            JarvisbotSongfinder.configure do |c|
              c.length_min = 0
            end
          end
          let(:subj) { klass.new }
          it "has a proper error" do
            # have to do multiple expectations in a single spec to fill errors
            expect(subj.valid?).to be false
            expect(subj.errors).to include(JarvisbotSongfinder::ReplyMessage::Request.too_short)
          end
        end
        context "when too long" do
          before do
            # make sure configuration initialized
            JarvisbotSongfinder.configure do |c|
              c.length_max = 200
            end
          end
          after do
            JarvisbotSongfinder.configure do |c|
              c.length_max = Float::INFINITY
            end
          end
          let(:subj) { klass.new }
          it "has a proper error" do
            expect(subj.valid?).to be false
            expect(subj.errors).to include(JarvisbotSongfinder::ReplyMessage::Request.too_long)
          end
        end
      end
    end
  end
end
