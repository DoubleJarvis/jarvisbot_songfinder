# frozen_string_literal: true

module JarvisbotSongfinder
  RSpec.describe JarvisbotSongfinder::UnknownProvider do
    let(:track) { described_class.new }
    it "is invalid" do
      expect(track.valid?).to eq(false)
    end
    it "has a proper error message" do
      expect(track.errors).to include(ReplyMessage::Request.unknown_provider)
    end
  end
end
