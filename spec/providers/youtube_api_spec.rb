# frozen_string_literal: true

raise "Make sure you have youtube_api_key configured" unless JarvisbotSongfinder.configuration.youtube_api_key

module JarvisbotSongfinder
  RSpec.describe JarvisbotSongfinder::YoutubeAPI do
    before(:all) do
      JarvisbotSongfinder.configure do |c|
        c.length_min = 0 
        c.length_max = 600
      end
    end
    after(:all) do
      JarvisbotSongfinder.configure do |c|
        c.length_min = 0 
        c.length_max = Float::INFINITY
      end
    end
    context "when valid video requested" do
      let(:video) do
        VCR.use_cassette("youtube:valid") do
          described_class.new('https://www.youtube.com/watch?v=oyERJwubHWI')
        end
      end

      it "returns true for #valid?" do
        expect(video.valid?).to be_truthy
      end
      it "has no #errors" do
        expect(video.errors).to be_empty
      end
      it "has a #length" do
        expect(video.length).not_to be_nil
      end
      it "has a title" do
        expect(video.title).not_to be_empty
      end
      it "has an artist" do
        expect(video.artist).not_to be_empty
      end
      it "has an url" do
        expect(video.url).not_to be_empty
      end
      it "responds to #provider" do
        expect(video.provider).to eq("youtube")
      end
      it 'responds to #explicit?' do
        expect(video.explicit?).to be_falsey
      end
    end

    context "invalid video requested" do
      context "when id is invalid" do
        let(:video) do
          VCR.use_cassette("youtube:invalid") do
            described_class.new('https://www.youtube.com/watch?v=nonexistent')
          end
        end
        it "is invalid" do
          expect(video.valid?).to eq(false)
        end
        it "has proper error" do
          expect(video.errors).to include(ReplyMessage::Request.invalid_video_id)
        end
      end
      context "when video is too long" do
        let(:video) do
          VCR.use_cassette("youtube:invalid:too_long") do
            described_class.new('https://www.youtube.com/watch?v=0PNmwk2Tr_c')
          end
        end

        it "is invalid" do
          expect(video.valid?).to be_falsey
        end
        it "has proper error" do
          expect(video.errors).to include(ReplyMessage::Request.too_long)
        end
      end
      context "when video is not available in the region" do
        let(:video) do
          VCR.use_cassette("youtube:invalid:region_restricted") do
            described_class.new('https://www.youtube.com/watch?v=DMFAGv2uKv0')
          end
        end

        it "is invalid" do
          expect(video.valid?).to be_falsey
        end
        it "has proper error" do
          expect(video.errors).to include(ReplyMessage::Request.region_restricted(@region))
        end
      end
      context "when video is not in the music category" do
        let(:video) do
          VCR.use_cassette("youtube:invalid:wrong_category") do
            described_class.new('https://www.youtube.com/watch?v=lQvcoOzmLqs')
          end
        end

        it "is invalid" do
          expect(video.valid?).to be_falsey
        end
        it "has proper error" do
          expect(video.errors).to include(ReplyMessage::Request.invalid_category)
        end
      end
    end
    describe "YoutubeAPI.from_search" do
      context "when provier has a track for given search term" do
        let(:track) do
          VCR.use_cassette("youtube:valid:search") do
            described_class.from_search('In Flames - Alias')
          end
        end
        it "returns a YoutubeAPI instance" do
          expect(track.class).to eq(JarvisbotSongfinder::YoutubeAPI)
        end
        it "returned track has a title" do
          expect(track.title).not_to be_nil
        end
      end
      context "when provider doesn't have a track for given search term" do
        let(:track) do
          VCR.use_cassette("youtube:invalid:search") do
            described_class.from_search('asdjflkasdfasd')
          end
        end
        it "returns nil" do
          expect(track).to be_nil
        end
      end
    end
  end
end
