# frozen_string_literal: true

module JarvisbotSongfinder
  RSpec.describe JarvisbotSongfinder::SpotifyAPI do
    before(:all) do
      JarvisbotSongfinder.configure do |c|
        c.length_min = 0 
        c.length_max = 600
      end
    end
    after(:all) do
      JarvisbotSongfinder.configure do |c|
        c.length_min = 0 
        c.length_max = Float::INFINITY
      end
    end
    context "when valid track requested" do
      # https://open.spotify.com/track/2EuqIRgQ3W7ttUCFSe2OLT?si=Zc1Z2lh-SX6ERgmK5VYG0Q
      let(:track) do
        VCR.use_cassette("spotify:valid") do
          described_class.new('https://open.spotify.com/track/0SGB0nFDl61RLJWwlZusla')
        end
      end

      it "returns true for #valid?" do
        expect(track.valid?).to be_truthy
      end
      it "has no #errors" do
        expect(track.errors).to be_empty
      end
      it "has a #length" do
        expect(track.length).not_to be_nil
      end
      it "has a title" do
        expect(track.title).not_to be_empty
      end
      it "has an artist" do
        expect(track.artist).not_to be_empty
      end
      it "has an url" do
        expect(track.url).not_to be_empty
      end
      it "responds to #provider" do
        expect(track.provider).to eq("spotify")
      end
      it 'responds to #explicit?' do
        expect(track.explicit?).to be_in([true, false])
      end
      context "when explicit track is requested" do
        let(:explicit) do
          VCR.use_cassette("spotify:valid:explicit") do
            described_class.new('https://open.spotify.com/track/3GCdLUSnKSMJhs4Tj6CV3s')
          end
        end
        it "returns true for #explicit?" do
          expect(explicit.explicit?).to be_truthy
        end
      end
      context "when clean track or unknown is requested" do
        let(:clean) do
          VCR.use_cassette("spotify:valid:clean_or_unknown") do
            described_class.new('https://open.spotify.com/track/19YPGbCVBVOoK2Ynfkb06W')
          end
        end
        it "returns false for #explicit?" do
          expect(clean.explicit?).to be_falsey
        end
      end
    end

    context "when invalid track requested" do
      context "when id is invalid" do
        let(:track) do
          VCR.use_cassette("spotify:invalid") do
            described_class.new('https://open.spotify.com/track/someshit')
          end
        end

        it "is invalid" do
          expect(track.valid?).to eq(false)
        end
        it "has proper error" do
          expect(track.errors).to include(ReplyMessage::Request.invalid_video_id)
        end
      end
      context "when track is too long" do
        let(:track) do
          VCR.use_cassette("spotify:invalid:too_long") do
            described_class.new('https://open.spotify.com/track/3U0NJKJIuWbtWVciXbw6hi')
          end
        end

        it "is invalid" do
          expect(track.valid?).to be_falsey
        end
        it "has proper error" do
          expect(track.errors).to include(ReplyMessage::Request.too_long)
        end
      end

      context "when track is not available in the region" do
        let(:track) do
          VCR.use_cassette("spotify:invalid:region_restricted") do
            described_class.new('https://open.spotify.com/track/2oL0JRZnH7ADg3qUH7P8mw')
          end
        end

        it "is invalid" do
          expect(track.valid?).to be_falsey
        end
        it "has a proper error" do
          expect(track.errors).to include(ReplyMessage::Request.region_restricted("US"))
        end
      end

      context "when track is not available in the region but can be url_relinked" do
        let(:track) do
          VCR.use_cassette("spotify:invalid:url_relinked") do
            described_class.new("https://open.spotify.com/track/76yVBx2iflWLKLAtZtOCug")
          end
        end
        it "is valid" do
          expect(track.valid?).to be true
        end
        it "has relinked url" do
          expect(track.url).not_to match %r{76yVBx2iflWLKLAtZtOCug}
        end
      end
    end

    describe "SpotifyAPI::from_search" do
      context "when provider has a track for given search term" do
        let(:track) do
          VCR.use_cassette("spotify:valid:search") do
            described_class.from_search('In Flames - Alias')
          end
        end
        it "returns a SpotifyAPI instance" do
          expect(track.class).to eq(JarvisbotSongfinder::SpotifyAPI)
        end
        it "returned track has a proper title" do
          expect(track.title).to eq("Alias")
        end
      end
      context "when provider doesn't have a track for given search term" do
        let(:track) do
          VCR.use_cassette("spotify:invalid:search") do
            described_class.from_search('Trove Seveyan - My My My')
          end
        end
        it "returns nil" do
          expect(track).to be_nil
        end
      end
    end
  end
end
