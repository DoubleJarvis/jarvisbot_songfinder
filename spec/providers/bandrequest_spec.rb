# frozen_string_literal: true

RSpec.describe JarvisbotSongfinder::Bandrequest do
  let(:request) { described_class.new("Paramore") }
  it "is valid" do
    expect(request.valid?).to be true
  end
  it "has stubbed out length" do
    expect(request.length).to eq 0
  end
  it "has stubbed out title" do
    expect(request.title).to be_empty
  end
  it "has an artist" do
    expect(request.artist).to eq "Paramore"
  end
  it "has a provider" do
    expect(request.provider).to eq "bandrequest"
  end
  it "has a url" do
    expect(request.url).to eq "https://www.last.fm/search?q=Paramore"
  end
  it "is not explicit" do
    expect(request.explicit?).to be false
  end
end
