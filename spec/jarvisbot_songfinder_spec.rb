# frozen_string_literal: true

RSpec.describe JarvisbotSongfinder do
  it "has a version number" do
    expect(JarvisbotSongfinder::VERSION).not_to be nil
  end
end
