## [1.1.1] - March 20, 2019
### Changes
- Fixed region restricted message having region code in it

## [1.1.0] - March 20, 2019
### Changes
- Configuation file can be injected per Query `JarvisbotSongfinder::Query.new("Paramore", config: conf)` where conf is something that responds to `.region` `.length_min` and `.length_max`. If nothing is injected - default app wide config will be used

## [1.0.3] - March 14, 2019
### Changes
- Explicitly specifying market in SpotifyAPI to utilize [track relinking](https://developer.spotify.com/documentation/general/guides/track-relinking-guide/)

## [1.0.2] - February 01, 2019
### Changed
- Fixed changelog url in gemspec

## [1.0.1] - February 01, 2019
### Changed
- Track length restrictions can now be configured via config.length_max and config.length_min (in seconds)

## [1.0.0] - February 01, 2019
### Initial release 